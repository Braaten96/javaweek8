package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class Main {
    public String URL = "https://pokeapi.co/api/v2/pokemon/";

    public Main() {
    }

    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the ID or the name of the pokemon you are searching for: ");
        String userInput = scan.nextLine();
        Main inst = new Main();
        inst.getPokemonInfo(userInput);
    }

    public JSONObject getPokemon(URL pokemonURL) throws IOException {
        JSONObject data = null;
        String line = null;
        HttpURLConnection connect = (HttpURLConnection)pokemonURL.openConnection();
        connect.setRequestMethod("GET");
        int respCode = connect.getResponseCode();
        if (respCode == 200) {
            BufferedReader br = new BufferedReader(new InputStreamReader(connect.getInputStream()));
            StringBuffer resp = new StringBuffer();

            while((line = br.readLine()) != null) {
                resp.append(line);
            }

            br.close();
            data = new JSONObject(resp.toString());
        } else {
            System.out.println("It didn't work!");
        }

        return data;
    }

    public void getPokemonInfo(String userInput) throws IOException {
        Main inst = new Main();
        URL url = new URL(this.URL + userInput);
        String typeURL = "";
        JSONObject data = inst.getPokemon(url);
        if (data != null) {
            String pokemonName = data.getString("name");
            int pokemonHeight = data.getInt("height");
            int pokemonWeight = data.getInt("weight");
            System.out.println("You chose: " + pokemonName);
            System.out.println("Weight " + pokemonWeight);
            System.out.println("Weight " + pokemonHeight);
            JSONArray types = data.getJSONArray("types");

            for(int i = 0; i < types.length(); ++i) {
                JSONObject type = types.getJSONObject(i).getJSONObject("type");
                String typeName = type.getString("name");
                typeURL = type.getString("url");
                System.out.println();
                System.out.println("Here is the pokemons who has matching type of " + typeName);
                System.out.println();
                inst.getPokemonsOfType(typeURL);
            }
        } else {
            System.out.println("There is no pokemon with that ID or name");
        }

    }

    public void getPokemonsOfType(String URL) throws IOException {
        Main inst = new Main();
        URL url = new URL(URL);
        JSONObject data = inst.getPokemon(url);
        JSONArray pokemons = data.getJSONArray("pokemon");

        for(int i = 0; i < pokemons.length(); ++i) {
            JSONObject pokemon = pokemons.getJSONObject(i).getJSONObject("pokemon");
            String pokemonName = pokemon.getString("name");
            System.out.println(pokemonName);
        }

    }
}
