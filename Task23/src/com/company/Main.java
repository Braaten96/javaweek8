package com.company;

import java.sql.*;

public class Main {

    private Connection connect(){
        String URL = "jdbc:sqlite:resources/Northwind_small.sqlite";
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(URL);
            System.out.println("Connection to database has been established.");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
    }

    public void selectProduct(){
        String sql = "SELECT p.Id, ProductName, CategoryName, Description " +
                "FROM Product p INNER JOIN Category c ON p.CategoryId=c.Id WHERE p.Id < 10";
        try(Connection conn = this.connect();
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(sql))
        {
            while (resultSet.next()){
                System.out.println("ProductId: " +resultSet.getInt("Id") + "\t"
                        + "Productname: " + resultSet.getString("ProductName") + "\t"
                        + "CategoryName: " + resultSet.getString("CategoryName") + "\t"
                        + "Description: " + resultSet.getString("Description"));
            }
            try{
                conn.close();
                System.out.println("Database connection is closed");
            }catch (SQLException ex){
                System.out.println(ex.getMessage());
            }
        }catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
	    Main main = new Main();
	    main.selectProduct();
    }
}
