package animals;
import java.util.*;

public class Carnivore extends Animal {

	public Carnivore(String name, ArrayList<Moveable> moves){
		super(name, moves);
	}

	public String type() {
		return " is a Carnivore";
	}

}
