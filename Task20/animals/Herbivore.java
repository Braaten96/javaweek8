package animals;
import java.util.*;

public class Herbivore extends Animal {

	public Herbivore(String name, ArrayList<Moveable> moves){
		super(name, moves);
	}

	public String type() {
		return " is a Herbivore";
	}

}
